package SmartDocs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static util.DriverManager.driver;

/**
 * Created by naveen on 11/5/17.
 */
public class Documents {
    @FindBy(how = How.LINK_TEXT, using = "Smart Docs")
    WebElement smartDocs;
    @FindBy(how = How.LINK_TEXT,using = "Documents")
    WebElement documents;
    @FindBy(how = How.XPATH,using = "//*[@id=\'search-container\']/a/i")
    WebElement addDocuments;
    @FindBy(how = How.ID,using = "name")
    WebElement name;
    @FindBy(how = How.NAME,using = "expiry_date")
    WebElement expiryDate;
    @FindBy(how = How.ID,using = "folder_id")
    WebElement folderId;
    @FindBy(how = How.ID,using = "client")
    WebElement client;
    @FindBy(how = How.NAME,using = "short_desc")
    WebElement shortDesc;
    @FindBy(how = How.NAME,using = "long_desc")
    WebElement longDesc;
    @FindBy(how = How.NAME,using = "shared")
    WebElement shared;
    @FindBy(how = How.ID,using = "browseUsersBox")
    WebElement permission;
    @FindBy(how = How.ID,using = "tags")
    WebElement tags;
    @FindBy(how = How.NAME,using = "meta_tags")
    WebElement metaTags;
    @FindBy(how = How.NAME,using = "linked_doc_id")
    WebElement linkedDocId;
    @FindBy(how = How.XPATH,using = "//*[@id=\'upload_files\']")
    WebElement uploadFile;
    @FindBy(how = How.XPATH,using = "//*[@id=\'formClient\']/div/form/div[14]/div/input[5]")
    WebElement submitButton;



    public void setSmartDocs(){
        smartDocs.click();
    }
    public void setDocuments(){
        documents.click();
    }
    public void setAddDocuments(){
        addDocuments.click();
    }
    public void setName(String nam){
        name.sendKeys(nam);
    }
    public void setExpiryDate(){
        expiryDate.click();
        List<WebElement> allDates=driver.findElements(By.xpath("/html/body/div[5]/div/div[3]/ul/li/div"));

        for(WebElement ele:allDates)
        {

            String date=ele.getText();

            if(date.equalsIgnoreCase("2"))
            {
                ele.click();
                break;
            }

        }

    }
    public void setFolderid(String folderid){
        Select status=new Select(folderId);
        status.selectByVisibleText(folderid);
    }

    public void setClientid(String cli){

        Select status=new Select(client);
        status.selectByVisibleText(cli);
    }
    public void setShortDesc(String shdes){
        shortDesc.sendKeys(shdes);
    }
    public void setLongDesc(String lgdes){
        longDesc.sendKeys(lgdes);
    }
    public void setShared(String shar){
        Select status=new Select(shared);
        status.selectByVisibleText(shar);
    }

    public void setPermission(String per) throws Exception
    {
        permission.sendKeys(per);
        Thread.sleep(10000);
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(By.xpath("//*[@id=\'wrapper\']/ul/li/span"))).doubleClick().build().perform();

    }
    public void setTags(String tag){
        tags.sendKeys(tag);
    }
    public void setMetaTags(String met){
    metaTags.sendKeys(met);
    }
    public void setLinkedDocId(String link){
    linkedDocId.sendKeys(link);
    }
    public void setUploadFile(String up){
        uploadFile.sendKeys(up);
    }
    public void setSubmitButton(){
        submitButton.click();
    }
}

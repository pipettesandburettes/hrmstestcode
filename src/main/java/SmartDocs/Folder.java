package SmartDocs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import static util.DriverManager.driver;

/**
 * Created by naveen on 11/5/17.
 */
public class Folder {

    @FindBy(how = How.LINK_TEXT, using = "Smart Docs")
    WebElement smartdocs;
    @FindBy(how = How.LINK_TEXT, using = "Folders")
    WebElement folder;
    @FindBy(how = How.XPATH, using = "//*[@id=\'search-container\']/a/i")
    WebElement addFolder;
    @FindBy(how = How.ID, using = "name")
    WebElement name;
    @FindBy(how = How.ID, using = "parent_id")
    WebElement parentId;
    @FindBy(how = How.ID, using = "client_id")
    WebElement clientid;
    @FindBy(how = How.ID, using = "shared")
    WebElement shared;
    @FindBy(how = How.ID, using = "browseUsersBox")
    WebElement permission;
    @FindBy(how = How.XPATH, using = "//*[@id=\'formClient\']/div/form/div[6]/div/input[4]")
    WebElement submitButton;


    public void setSmartdocs() {
        smartdocs.click();
    }

    public void setFolder() {
        folder.click();
    }

    public void setAddFolder() {
        addFolder.click();
    }

    public void setName(String nam) {
        name.sendKeys(nam);
    }

    public void setParentId(String parid)
    {
    Select status = new Select(parentId);
        status.selectByVisibleText(parid);
    }
    public void setClientid(String cli){
        Select status=new Select(clientid);
        status.selectByVisibleText(cli);

    }
    public void setShared(String sh){
        shared.sendKeys(sh);
    }
    public void setPermission(String per) throws InterruptedException {
        permission.sendKeys(per);
        Thread.sleep(10000);
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(By.xpath("//*[@id=\'wrapper\']/ul/li/span"))).doubleClick().build().perform();


    }
public void setSubmitButton(){
        submitButton.click();
}

    }




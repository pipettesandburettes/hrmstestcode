package pageobject.Admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by naveen on 7/4/17.
 */
public class Clients {
    //add client
    @FindBy(how = How.LINK_TEXT,using ="Admin")
    WebElement admin;
    @FindBy(how =How.LINK_TEXT, using="Clients")
    WebElement clients;
    @FindBy(how = How.XPATH, using = "//*[@id=\'search-container\']/a/i")
    WebElement addClient;
    @FindBy(how= How.ID, using= "name")
    WebElement name;
    @FindBy(how= How.ID, using= "group_id")
    WebElement group_dropdown;
    @FindBy(how= How.ID, using= "address")
    WebElement address;
    @FindBy(how= How.ID,using= "status")
    WebElement status_dropdown;
    @FindBy(how= How.ID, using= "lat")
    WebElement lat;
    @FindBy(how= How.ID, using= "lang")
    WebElement lang;
    @FindBy(how= How.CLASS_NAME, using= "btn-primary")
    WebElement buton;

    //click on admin
    public void clickOnAdmin()
    {
        admin.click();
    }
    public void clickOnClients(){clients.click();}
    public void clickOnAddClient()
    {
        addClient.click();
    }
    public void clickOnName(String nam)
    {
        name.sendKeys(nam);
    }
    public void select_List(String grpid)
    {
        Select group_id=new Select(group_dropdown);
        group_id.selectByVisibleText(grpid);
    }
    public void enterAddress(String add)
    {
        address.sendKeys(add);
    }
    public void select_status(String stat)
    {
        Select status=new Select(status_dropdown);
        status.selectByVisibleText(stat);
    }
    public void enter_Lat(String l)
    {
        lat.sendKeys(l);
    }
    public void enter_long(String lng)
    {
        lang.sendKeys(lng);
    }
    public void clickOnSubmit()
    {
        buton.click();
    }


}



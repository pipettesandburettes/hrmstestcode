package pageobject.Admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by naveen on 7/4/17.
 */
public class Groups {
    @FindBy(how = How.LINK_TEXT, using = "Admin")
    WebElement admin;
    @FindBy(how = How.LINK_TEXT, using = "Groups")
    WebElement groups;
    @FindBy(how = How.XPATH, using = "//*[@id=\'search-container\']/a/i")
    WebElement addGroups;
    @FindBy(how = How.ID, using = "name")
    WebElement name;
    @FindBy(how = How.ID, using = "client")
    WebElement client_dropdown;
    @FindBy(how = How.ID, using = "shared")
    WebElement shared_dropdown;


    public void setAdmin() {
        admin.click();
    }

    public void setGroups() {
        groups.click();
    }

    public void setAddGroups() {
        addGroups.click();
    }

    public void setName(String nam) {
        name.sendKeys(nam);
    }

    public void setClient(String cliid) {
        Select client_id = new Select(client_dropdown);
        client_id.selectByVisibleText(cliid);
    }
    public void setShared(String shreid){
        Select shared_id = new Select(shared_dropdown);
        shared_id.selectByVisibleText(shreid);
    }
}


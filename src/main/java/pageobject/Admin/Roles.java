package pageobject.Admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by naveen on 7/4/17.
 */
public class Roles {
    @FindBy(how = How.LINK_TEXT, using="Admin")
    WebElement admin;
    @FindBy(how =How.LINK_TEXT, using="Roles")
    WebElement roles;
    @FindBy(how =How.XPATH, using="//*[@id=\'search-container\']/a/i")
    WebElement clickOnrole;
    @FindBy(how = How.ID, using ="name")
    WebElement name;
    @FindBy(how =How.ID, using="client")
    WebElement client_dropdown;
    @FindBy(how =How.XPATH,using ="//*[@id=\'addUser\']/div/form/div[3]/div/input[4]")
    WebElement clickOnButton;
    public void clickOnAdmin()
    {
        admin.click();
    }
    public void setClickOnrole(){ roles.click();}
    public void clickOnRole()
    {
        clickOnrole.click();
    }

    public void enterName(String nam)
    {
        name.sendKeys(nam);
    }
    public void select_status(String stat)
    {
        Select status=new Select(client_dropdown);
        status.selectByVisibleText(stat);
    }
    public void setClickOnButton()
    {
        clickOnButton.click();
    }

}






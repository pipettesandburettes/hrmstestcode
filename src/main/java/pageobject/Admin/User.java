package pageobject.Admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import static util.DriverManager.driver;

/**
 * Created by naveen on 7/4/17.
 */
public class User {
    @FindBy(how = How.LINK_TEXT,using= "Admin")
    WebElement admin;
    @FindBy(how =How.LINK_TEXT,using ="Users")
    WebElement users;
    @FindBy(how=How.XPATH,using= "//*[@id=\'search-container\']/a/i")
    WebElement clickOnUser;
    @FindBy(how = How.ID, using ="username")
    WebElement username;
    @FindBy(how=How.XPATH,using ="//*[@id=\'adduser\']/div/form/div[2]/div/div/label")
    WebElement available;
    @FindBy(how =How.ID, using= "password")
    WebElement password;
    @FindBy(how =How.ID,using ="name")
    WebElement name;
    @FindBy(how =How.ID, using="nickname")
    WebElement nickname;
    @FindBy(how=How.ID,using ="role")
    WebElement role_dropdown;
    @FindBy(how = How.ID,using = "employee_name")
    WebElement employeename;

    @FindBy(how =How.ID,using= "email")
    WebElement email;
    @FindBy(how =How.CLASS_NAME, using ="btn-primary")
    WebElement button;
    public void clickOnAdmin()
    {
        admin.click();
    }
    public void setClickOnUser() { users.click(); }
    public void setAddUser()
    {
        clickOnUser.click();
    }
    public void setUsername(String user)
    {
        username.sendKeys(user);
    }
    public void setAvalilable(){available.click();}
    public void setPassword(String pass)
    {
        password.sendKeys(pass);
    }
    public void setName(String nam)
    {
        name.sendKeys(nam);
    }
    public void setNickname(String nick)
    {
        nickname.sendKeys(nick);
    }
    public void select_status(String stat)
    {
        Select status=new Select(role_dropdown);
        status.selectByVisibleText(stat);
    }
    public void setEmployeename(String cat) throws InterruptedException {
        employeename.sendKeys(cat);
//        WebDriverWait wait = new WebDriverWait(driver, 1000);
        Thread.sleep(10000);
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(By.xpath("//*[@id=\'adduser\']/div/form/div[7]/div/ul/li/span"))).doubleClick().build().perform();


    }
    public void setEmail(String emai)
    {
        email.sendKeys(emai);
    }
    public void setSubmit() { button.submit(); }

}





package pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by naveen on 3/4/17.
 */
public class Company {
    //identify locators of company
    @FindBy(how = How.LINK_TEXT,using = "Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT,using = "Company")
    WebElement company;


    @FindBy(how = How.XPATH,using = "//*[@id=\'innerContent\']/div[2]/a/i")
    WebElement addUser;
    @FindBy(how = How.ID,using = "name")
    WebElement name;
    @FindBy(how = How.NAME,using = "client_id")
    WebElement clientid;




    @FindBy(how = How.XPATH,using = "//*[@id=\'name_locale\']")
    WebElement namLocal;

    @FindBy(how = How.ID, using = "address")
    WebElement address;
    @FindBy(how = How.XPATH,using = "//*[@id=\"addcompanydetails\"]/div/form/div[5]/div/input[4]")
    WebElement submitButton;

    public void  setGlobalInterface(){ globalInterface.click();}
    public  void setCompany(){
        company.click();
    }

    public  void setAddUser(){
        addUser.click();
    }
    public void setName(String nam){
        name.sendKeys(nam);
    }
    public void setClientid(String cli){

        Select status=new Select(clientid);
        status.selectByVisibleText(cli);
    }
    public void setNamLocal(String loc){
        namLocal.sendKeys(loc);
    }
    public void setAddress(String add){
        address.sendKeys(add);

    }

    public void setSubmitButton(){
        submitButton.click();
    }


}

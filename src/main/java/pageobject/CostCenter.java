package pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by naveen on 3/4/17.
 */
public class CostCenter {
    @FindBy(how = How.LINK_TEXT,using = "Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT,using = "Cost Center")
    WebElement costCenter;
    @FindBy(how = How.XPATH,using = "//*[@id=\'innerContent\']/div[2]/a/i")
    WebElement addUser;
    @FindBy(how = How.ID, using = "name")
    WebElement name;
    @FindBy(how = How.XPATH,using = "//*[@id=\'name_locale\']")
    WebElement namLocal;
    @FindBy(how = How.NAME,using = "client_id")
    WebElement clientid;
    @FindBy(how = How.XPATH,using = "//*[@id=\"addcostcenter\"]/div/form/div[4]/div/input[4]")
    WebElement submitButton;



    public void setGlobalInterface(){
        globalInterface.click();
    }
    public void setCostCenter(){
        costCenter.click();
    }
    public void setAddUser(){
        addUser.click();
    }
    public void setName(String nam){
        name.sendKeys(nam);
    }
    public void setNamLocal(String naml){namLocal.sendKeys(naml);}
    public void setClientid(String cli){

        Select status=new Select(clientid);
        status.selectByVisibleText(cli);
    }
    public void setSubmitButton(){
        submitButton.click();
    }
}

package pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by naveen on 3/4/17.
 */
public class Department {
    @FindBy(how = How.LINK_TEXT,using = "Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT, using = "Department")
    WebElement department;
    @FindBy(how = How.XPATH,using = "//*[@id=\'innerContent\']/div[2]/a/i")
    WebElement addUser;
    @FindBy(how = How.ID, using = "name")
    WebElement name;
    @FindBy(how = How.ID,using = "parent_department")
    WebElement parent_department;
    @FindBy(how = How.XPATH,using = "//*[@id=\'adddepartment\']/div/form/div[3]/div/input[4]")
    WebElement submitButton;

    public  void setGlobalInterface(){
        globalInterface.click();
    }
    public void setDepartment(){
        department.click();
    }
    public void setAddUser(){
        addUser.click();
    }
    public void setName(String nam){
        name.sendKeys(nam);
    }
    public void setParent_department(String par){
        parent_department.sendKeys(par);
    }
    public void setSubmitButton(){
        submitButton.click();
    }

}

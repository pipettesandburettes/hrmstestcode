package pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by naveen on 3/4/17.
 */
public class Designation {
    //identify locators of designation page
    @FindBy(how = How.LINK_TEXT,using = "Global Interface")
    WebElement globalInter;
    @FindBy(how = How.LINK_TEXT,using = "Designation")
    WebElement designation;
    @FindBy(how = How.XPATH,using = "//*[@id=\"search-container\"]/a/i")
    WebElement addUser;
    @FindBy(how = How.ID,using = "name")
    WebElement name;
    @FindBy(how = How.ID,using = "name_locale")
    WebElement name_local;
    @FindBy(how = How.ID, using = "client_id")
    WebElement clientid;
    @FindBy(how = How.ID,using = "job_description")
    WebElement jobDescription;

    @FindBy(how = How.NAME,using = "parent_department")
    WebElement parentdepartment;

    @FindBy(how = How.NAME,using = "level")
    WebElement level;


    @FindBy(how = How.XPATH,using = "//*[@id=\"adddesignation\"]/div/form/div[8]/div/input[4]")
    WebElement submitButton;


    public void setGlobalInter(){
        globalInter.click();
    }
    public void setDesignation(){
        designation.click();
    }
    public void setAddUser(){
        addUser.click();
    }
    public void setName(String nam){
        name.sendKeys(nam);
    }
    public void setName_local(String naml){name_local.sendKeys(naml);}
    public void setClientid(String cli){
        Select status=new Select(clientid);
        status.selectByVisibleText(cli);

    }
    public void setJobDescription(String  job){
        Select status=new Select(jobDescription);
        status.selectByVisibleText(job);
    }

    public void setParentdepartment(String par)
    {
        Select status=new Select(parentdepartment);
        status.selectByVisibleText(par);
    }


    public void setLevel(String lev){
        Select status=new Select(level);
        status.selectByVisibleText(lev);
    }



    public void setSubmitButton(){
        submitButton.click();
    }
}

package pageobject.Edit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by naveen on 11/5/17.
 */
public class BankAccountEdit {

    //identify locators for bankaccount page

    @FindBy(how= How.LINK_TEXT, using ="Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT,using = "Bank And Bank Accounts")
    WebElement bankAccount;
    @FindBy(how =How.XPATH, using ="//*[@id=\"search-container\"]/a/i")
    WebElement addBankAccount;
    @FindBy(how =How.ID, using = "bank_name")
    WebElement bankName;
    @FindBy(how = How.ID,using = "name_locale")
    WebElement namelocal;

    @FindBy(how = How.ID,using = "client_id")
    WebElement clientid;

    @FindBy(how = How.ID,using = "branch_name")
    WebElement branchName;
    @FindBy(how = How.ID,using ="address")
    WebElement address;
    @FindBy(how = How.ID,using = "ifsc_code")
    WebElement ifscCode;
    @FindBy(how = How.ID,using = "phone_no")
    WebElement phoneNo;
    @FindBy(how = How.ID, using = "account_type_id")
    WebElement accountTypeId;
    @FindBy(how = How.ID,using = "account_no")
    WebElement accountNo;
    @FindBy(how = How.ID,using = "iban_no")
    WebElement ibanNo;
    @FindBy(how = How.XPATH,using = "//*[@id=\'addbankdetails\']/div/form/div[11]/div/input[6]")
    WebElement submitButton;
    //*[@id="addbankdetails"]/div/form/div[11]/div/input[6]
    public void setGlobalInterface(){
        globalInterface.click();
    }
    public void setBankAccount(){bankAccount.click();}
    public  void setAddBankAccount(){
        addBankAccount.click();
    }
    public void setBankName(String bankname,boolean isclear){
    if(isclear)
        bankName.clear();
        bankName.sendKeys(bankname);
    }
    public void setNamelocal(String namloc,  boolean isclear){
        if(isclear)

        namelocal.clear();
        namelocal.sendKeys(namloc);}
    public void setClientid(String cli){

        Select status=new Select(clientid);
        status.selectByVisibleText(cli);
    }

    public void setBranchName(String branchNam, boolean isclear){

      if(isclear)
        branchName.clear();
        branchName.sendKeys(branchNam);
    }
    public void setAddress(String add, boolean isclear){

        if(isclear)
        address.clear();
        address.sendKeys(add);
    }
    public void setIfscCode(String ifsc, boolean isclear){

        if(isclear)
        ifscCode.clear();
        ifscCode.sendKeys(ifsc);
    }
    public void setPhoneNo(String phone, boolean isclear){

        if(isclear)
        phoneNo.clear();
        phoneNo.sendKeys(phone);
    }
    //Drop down list

    public void select_status(String stat)
    {
        Select status=new Select(accountTypeId);
        status.selectByVisibleText(stat);
    }

    public void setAccountNo(String accno, boolean isclear){

        if(isclear)
        accountNo.clear();
        accountNo.sendKeys(accno);
    }
    public  void setIbanNo(String iban, boolean isclear)
    {
        if(isclear)
        ibanNo.clear();
        ibanNo.sendKeys(iban);
    }

    public String getBankName() {
        return bankName.getAttribute("value");
    }



    public void setSubmitButton(){
        submitButton.click();
    }

}





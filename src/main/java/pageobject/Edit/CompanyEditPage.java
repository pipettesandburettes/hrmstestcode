package pageobject.Edit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by naveen on 17/4/17.
 */
public class CompanyEditPage {

    @FindBy(how = How.LINK_TEXT,using = "Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT,using = "Company")
    WebElement company;
    @FindBy(how = How.XPATH,using = "//*[@id=\'list-container\']/ul/li[2]/span[1]/a/i")
    WebElement clickonButton;
    @FindBy(how = How.LINK_TEXT,using = "Edit")

    WebElement clickonEdit;

    @FindBy(how = How.ID,using = "name")
    WebElement name;
    @FindBy(how = How.ID, using = "address")
    WebElement address;
    @FindBy(how = How.ID,using = "name_locale")
    WebElement namLocal;
    @FindBy(how = How.XPATH,using = "//*[@id=\"addcompanydetails\"]/div/form/div[4]/div/input[4]")
    WebElement submitButton;




    public void  setGlobalInterface(){ globalInterface.click();}
    public  void setCompany(){
        company.click();
    }
    public void setClickonButton(WebElement ele){ele.click();}
    public void setClickonEdit(WebElement ele){ele.click();}

    public void setName(String nam, boolean isClear){
        if(isClear)
            name.clear();
        name.sendKeys(nam);

    }
    public void setAddress(String add, boolean isClear){
        if(isClear)
            address.clear();

        address.sendKeys(add);

    }
    public void setNamLocal(String loc, boolean isClear){
        if(isClear)
            namLocal.clear();
        namLocal.sendKeys(loc);
    }


    public String getName(){
        return name.getAttribute("value");

    }
    public String getAddress(){
        return address.getAttribute("value");

    }
    public String getNamLocal(){
        return namLocal.getAttribute("value");
    }

    public void setSubmitButton() {
        submitButton.click();
    }


}





package pageobject.EmployeeSelfService;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by naveen on 5/4/17.
 */
public class AddGrievance {
    @FindBy(how = How.LINK_TEXT,using = "Employee")
    WebElement employee;
    @FindBy(how = How.LINK_TEXT,using = "Employee Self Service")
    WebElement selfService;
    @FindBy(how = How.LINK_TEXT,using = "Grievnace Redressal")
    WebElement grievanceRedressal;
    @FindBy(how = How.XPATH,using = "//*[@id=\'search-container\']/a/i")
    WebElement addGrievance;
    @FindBy(how = How.ID,using = "emp_id")
    WebElement empId;
    @FindBy(how = How.NAME,using = "grievance_type_id")
    WebElement grievanceType;
    @FindBy(how = How.NAME,using = "report_date")
    WebElement reportDate;
    @FindBy(how = How.ID,using = "description")
    WebElement description;
    @FindBy(how = How.XPATH,using = "//*[@id=\'formGrievance\']/div/form/div[5]/div/input[4]")
    WebElement submitButton;


    public void setEmployee(){
        employee.click();
    }
    public void setSelfService(){
        selfService.click();
    }
    public void setGrievanceRedressal(){
        grievanceRedressal.click();
    }
    public void setAddGrievance(){
        addGrievance.click();
    }

    public void setEmpId(String stat){
        Select status =new Select(empId);
        status.selectByVisibleText(stat);
    }
    public void setGrievanceType(String stat){
        Select status =new Select(grievanceType);
        status.selectByVisibleText(stat);
    }
    public void setReportDate(String date){
        reportDate.sendKeys(date);
    }
    public void setDescription(String des){
        description.sendKeys(des);
    }
    public void setSubmitButton(){
        submitButton.click();
    }



}

package pageobject.EmployeeSelfService;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static util.DriverManager.driver;

/**
 * Created by naveen on 5/4/17.
 */
public class ApplyforLeave {
    @FindBy(how = How.LINK_TEXT,using = "Employee")
    WebElement employee;
    @FindBy(how = How.LINK_TEXT,using = "Employee Self Service")
    WebElement selfService;
    @FindBy(how = How.LINK_TEXT,using = "Apply for leave")
    WebElement applyforLeave;
    @FindBy(how = How.XPATH,using = "//*[@id=\"search-container\"]/a/i")
    WebElement addleave;

    @FindBy(how = How.ID,using = "leave_type_id")
    WebElement leaveTypeId;
    @FindBy(how = How.ID,using = "from_date")
    WebElement fromDate;
    @FindBy(how = How.ID,using = "to_date")
    WebElement toDate;
    @FindBy(how = How.ID,using = "reason")
    WebElement reason;
    @FindBy(how = How.ID,using = "remarks")
    WebElement remarks;
    @FindBy(how = How.XPATH,using = "//*[@id=\'leave_submit\']")
    WebElement submitButton;
    @FindBy(how = How.LINK_TEXT,using = "Hello neetha vj")
    WebElement cliekOnUsername;
    @FindBy(how = How.LINK_TEXT,using = "Logout")
    WebElement clickOnLogout;







    public void setEmployee(){
        employee.click();
    }
    public void setSelfService(){
        selfService.click();
    }
    public void setApplyforLeave(){
        applyforLeave.click();
    }
    public void setAddleave(){
        addleave.click();
    }

    public void setLeaveTypeId(String leav){

        Select status =new Select(leaveTypeId);
        status.selectByVisibleText(leav);
    }
    public void setFromDate(){
    fromDate.click();
        List<WebElement> allDates=driver.findElements(By.xpath("/html/body/div[6]/div/div[3]/ul/li/div"));


        for(WebElement ele:allDates)
        {

            String date=ele.getText();

            if(date.equalsIgnoreCase("29"))
            {
                ele.click();
                break;
            }

        }
    }
    public void setToDate(){

        toDate.click();

        List<WebElement> allDates=driver.findElements(By.xpath("/html/body/div[6]/div/div[3]/ul/li/div"));


        for(WebElement ele:allDates)
        {

            String date=ele.getText();

            if(date.equalsIgnoreCase("29"))
            {
                ele.click();
                break;
            }

        }


    }

    public void setReason(String rea){
        reason.sendKeys(rea);
    }
    public void setRemarks(String rem){
    remarks.sendKeys(rem);
    }
public void setSubmitButton(){
    submitButton.click();
}

public void setCliekOnUsername(){
    cliekOnUsername.click();
}
public void setClickOnLogout(){
    clickOnLogout.click();
}




}





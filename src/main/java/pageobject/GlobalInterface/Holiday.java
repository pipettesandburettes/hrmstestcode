package pageobject.GlobalInterface;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static util.DriverManager.driver;

/**
 * Created by naveen on 10/4/17.
 */
public class Holiday {
    @FindBy(how = How.LINK_TEXT, using = "Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT,using = "Holiday Configuration")
    WebElement holiday;
    @FindBy(how = How.XPATH, using = "//*[@id=\'search-container\']/a/i")
    WebElement addHoliday;
    @FindBy(how = How.ID,using = "client_id")
    WebElement clientid;


    @FindBy(how = How.ID, using = "hol_date")
    WebElement hol_date;
    @FindBy(how = How.ID, using = "name")
    WebElement name;
    @FindBy(how = How.ID,using = "remark")
    WebElement remark;
    @FindBy(how = How.XPATH, using = "//*[@id=\"formHolidayConfig\"]/div/form/div[5]/div/input[4]")
    WebElement submitButton;


    public void setGlobalInterface(){
        globalInterface.click();
    }
    public void setHoliday(){
        holiday.click();
    }
    public void setAddHoliday(){
        addHoliday.click();
    }

    public void setClientid(String cli){

        Select status=new Select(clientid);
        status.selectByVisibleText(cli);
    }
    public void setHol_date(){


        hol_date.click();

        List<WebElement> allDates=driver.findElements(By.xpath("/html/body/div[5]/div/div[3]/ul/li/div"));

        for(WebElement ele:allDates)
        {

            String date=ele.getText();

            if(date.equalsIgnoreCase("28"))
            {
                ele.click();
                break;
            }

        }


    }










    public void setName(String nam){
        name.sendKeys(nam);
    }
    public void setRemark(String rem){
        remark.sendKeys(rem);
    }
    public  void setSubmitButton(){
        submitButton.click();
    }

}

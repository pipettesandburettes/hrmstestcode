package pageobject.GlobalInterface;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static util.DriverManager.driver;

/**
 * Created by naveen on 10/4/17.
 */
public class LeaveRule {
    @FindBy(how = How.LINK_TEXT, using = "Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT,using = "Leave Rule Configuration")
    WebElement leaveRule;
    @FindBy(how = How.XPATH, using = "//*[@id=\"search-container\"]/a/i")
    WebElement addLeaveRule;
    @FindBy(how = How.ID,using = "leave_type_id")
        WebElement leavetypeid;

    @FindBy(how = How.ID,using = "valid_from_date")
    WebElement validfromdate;
    @FindBy(how = How.ID,using = "valid_till_date")
    WebElement validtilldate;
    @FindBy(how = How.ID,using = "leave_credit_count")
    WebElement leavecreditcount;
    @FindBy(how = How.ID,using = "leave_credit_on")
    WebElement leaveCreditOn;
    @FindBy(how = How.ID,using = "leave_matured_on")
    WebElement leaveMaturedOn;
    @FindBy(how = How.ID,using = "clubbed_with_cl")
    WebElement clubbedWithCl;
    @FindBy(how = How.ID,using = "clubbed_with_holiday")
    WebElement clubbedWithHoliday;
    @FindBy(how = How.XPATH,using = "//*[@id=\'formLeaveRuleConfig\']/div/form/div[9]/div/input[4]")
    WebElement submitButton;


    public void setGlobalInterface(){
        globalInterface.click();
    }
    public void setLeaveRule(){
        leaveRule.click();
    }
    public void setAddLeaveRule(){
        addLeaveRule.click();
    }



    public void setLeavetypeid(String leavid){
        Select status=new Select(leavetypeid);
        status.selectByVisibleText(leavid);
    }
    public void setValidfromdate(){

        validfromdate.click();

        List<WebElement> allDates=driver.findElements(By.xpath("/html/body/div[5]/div/div[3]/ul/li/div"));

        for(WebElement ele:allDates)
        {

            String date=ele.getText();

            if(date.equalsIgnoreCase("2"))
            {
                ele.click();
                break;
            }

        }

    }
public void setValidtilldate() {
    validtilldate.click();


    List<WebElement> allDates = driver.findElements(By.xpath("/html/body/div[5]/div/div[3]/ul/li/div"));

    for (WebElement ele : allDates) {

        String date = ele.getText();

        if (date.equalsIgnoreCase("29")) {
            ele.click();
            break;
        }

    }
}

public void setLeavecreditcount(String credit){
    leavecreditcount.sendKeys(credit);
}
public void setLeaveCreditOn(String creditOn)
{
    Select status=new Select(leaveCreditOn);
    status.selectByVisibleText(creditOn);
}
public void setLeaveMaturedOn(String maturedOn){
    Select status=new Select(leaveMaturedOn);
    status.selectByVisibleText(maturedOn);
}
public void setClubbedWithCl(String clubbedWithCl1){
    Select status=new Select(clubbedWithCl);
    status.selectByVisibleText(clubbedWithCl1);

}

public void setClubbedWithHoliday(String clubbedWithHoliday1){
    Select status=new Select(clubbedWithHoliday);
    status.selectByVisibleText(clubbedWithHoliday1);


}
public void setSubmitButton(){
    submitButton.click();
}

}

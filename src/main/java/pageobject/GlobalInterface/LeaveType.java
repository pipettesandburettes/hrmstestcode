package pageobject.GlobalInterface;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by naveen on 10/4/17.
 */
public class LeaveType {
    @FindBy(how = How.LINK_TEXT,using = "Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT,using = "Leave Type Configuration")
    WebElement leavetype;
    @FindBy(how = How.XPATH, using =  "//*[@id=\'search-container\']/a/i")
    WebElement addleave;

    @FindBy(how = How.ID,using = "client_id")
    WebElement clientid;

    @FindBy(how = How.ID,using =  "leave_name")
    WebElement leaveName;
    @FindBy(how = How.ID, using = "leave_with_salary")
    WebElement leaveWithSalary;
    @FindBy(how = How.ID,using = "carry_forward")
    WebElement carryForward;
    @FindBy(how = How.XPATH,using = "//*[@id=\"formLeaveTypeConfig\"]/div/form/div[5]/div/input[4]")
    WebElement submitButton;

    public void setGlobalInterface(){
        globalInterface.click();
    }
    public void setLeavetype(){
        leavetype.click();
    }

    public void setAddleave(){
        addleave.click();
    }
    public void setClientid(String cli){

        Select status=new Select(clientid);
        status.selectByVisibleText(cli);
    }
    public void setLeave(String nam){
        leaveName.sendKeys(nam);
    }


    public void setLeaveName(String l)
    {
        Select leave=new Select(leaveWithSalary);
        leave.selectByVisibleText(l);
    }
    public void setCarryForward(String a){
        Select cary=new Select(carryForward);
        cary.selectByVisibleText(a);
    }
    public void setSubmitButton(){
        submitButton.click();
    }

}

package pageobject.GlobalInterface;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;

import static util.DriverManager.driver;

/**
 * Created by naveen on 11/5/17.
 */
public class ShiftConfiguration {
     @FindBy(how = How.LINK_TEXT, using = "Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT, using = "Shift Configuration")
    WebElement shiftConfiguration;
    @FindBy(how = How.XPATH,using = "//*[@id=\'search-container\']/a/i")
    WebElement addShiftConfig;
    @FindBy(how = How.ID,using = "shift_name")
    WebElement shiftName;
    @FindBy(how = How.ID,using = "name_locale")
    WebElement nameLOcal;
    @FindBy(how = How.ID,using = "client_id")
    WebElement clientid;
    @FindBy(how = How.ID,using = "start_time")
    WebElement startTime;
    @FindBy(how = How.ID,using = "break_start")
    WebElement breakTime;
    @FindBy(how = How.ID,using = "break_end")
    WebElement breakEnd;
    @FindBy(how = How.ID,using = "end_time")
    WebElement endTime;
    @FindBy(how = How.ID,using = "exception_time")
    WebElement exceptionTime;
    @FindBy(how = How.ID,using = "min_working_hours")
    WebElement minWorkingHours;
    @FindBy(how = How.XPATH,using = "//*[@id=\"addshiftconf\"]/div/form/div[10]/div/input[6]")
    WebElement submitButton;
    ArrayList tabs = new ArrayList (driver.getWindowHandles());

//Use the list of window handles to switch between windows


    public void setGlobalInterface(){
        globalInterface.click();
    }
    public void setShiftConfiguration(){
        shiftConfiguration.click();
    }
    public void setAddShiftConfig(){
        addShiftConfig.click();
    }
    public void setShiftName(String nam){
        shiftName.sendKeys(nam);
    }
    public void setNameLOcal(String naml){
        nameLOcal.sendKeys(naml);
    }
    public void setClientid(String cli){

        Select status=new Select(clientid);
        status.selectByVisibleText(cli);
    }
    public void setStartTime(){

        startTime.sendKeys("09:00:00");

    }
    public void setBreakTime(){

        breakTime.sendKeys("12:00:00");
    }
    public void setBreakEnd(){

        breakEnd.sendKeys("01:00:00");
    }
    public void setEndTime(){

        endTime.sendKeys("06:00:00");
    }

    public void setExceptionTime(String exp){
        exceptionTime.sendKeys(exp);
    }
    public void setMinWorkingHours(String min){
        minWorkingHours.sendKeys(min);
    }
    public void setSubmitButton(){
        submitButton.click();
    }


}

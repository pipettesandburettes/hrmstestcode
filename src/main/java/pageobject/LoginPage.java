package pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by naveen on 3/4/17.
 */
public class LoginPage {
    //identify the locators for login page


    @FindBy(how= How.NAME,using="username")
    WebElement username;
    @FindBy(how=How.NAME, using="password")
    WebElement password;
    @FindBy(how=How.XPATH, using="//*[@id=\'login-container\']/form/div[3]/div/p/input[1]")
    WebElement button;
    public void  userLogin(String user, String pass)
    {
        username.sendKeys(user);
        password.sendKeys(pass);
        button.click();
    }
}

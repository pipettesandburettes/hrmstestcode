package pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by naveen on 3/4/17.
 */
public class Nationality {

    //identify locators of nationality
    @FindBy(how = How.LINK_TEXT, using = "Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT, using = "Nationality")
    WebElement nationality;
    @FindBy(how = How.XPATH,using = "//*[@id=\'search-container\']/a/i")
    WebElement addUser;
    @FindBy(how = How.ID,using = "name")
    WebElement name;
    @FindBy(how = How.XPATH,using = "//*[@id=\'addnationality\']/div/form/div[2]/div/input[4]")
    WebElement submitButton;


    public void setGlobalInterface(){
        globalInterface.click();
    }
    public void setNationality(){
        nationality.click();
    }
    public void setAddUser(){
        addUser.click();
    }
    public void setName(String nam){
        name.sendKeys(nam);
    }
    public void setSubmitButton(){
        submitButton.click();
    }
}

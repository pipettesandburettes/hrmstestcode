package pageobject.Search;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by naveen on 19/4/17.
 */
public class CompanySearchPage {
    @FindBy(how = How.LINK_TEXT,using = "Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT,using = "Company")
    WebElement company;
    @FindBy(how = How.NAME,using = "name")
    WebElement name;
    @FindBy(how = How.NAME,using = "address")
    WebElement address;
    @FindBy(how = How.XPATH, using = "//*[@id=\'search-container\']/form/input")
    WebElement search;



    public void setGlobalInterface(){globalInterface.click();}
    public void setCompany(){company.click();}
    public void setName(String nam)
    {
        name.clear();
        name.sendKeys(nam);}

    public void setAddress(String add)
    {
        address.clear();
        address.sendKeys(add);
    }
    public void setSearch(){search.click();}



}

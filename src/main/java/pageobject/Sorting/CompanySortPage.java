package pageobject.Sorting;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by naveen on 18/4/17.
 */
public class CompanySortPage {
    @FindBy(how = How.LINK_TEXT,using = "Global Interface")
    WebElement globalInterface;
    @FindBy(how = How.LINK_TEXT,using = "Company")
    WebElement company;
    @FindBy(how = How.LINK_TEXT,using = "Company Name")
    WebElement companyName;
    @FindBy(how = How.LINK_TEXT,using = "Address")
    WebElement address;
    @FindBy(how = How.LINK_TEXT,using = "Name Locale")
    WebElement nameLocale;
    @FindBy(how = How.LINK_TEXT,using = "Created on")
    WebElement createdOn;
    @FindBy(how = How.LINK_TEXT,using = "Last Updated On")
    WebElement lastCreatedOn;




    public void  setGlobalInterface(){ globalInterface.click();}
    public  void setCompany(){
        company.click();
    }
    public void setCompanyName(){companyName.click();}
    public void setAddress(){address.click();}
    public void setNameLocale(){nameLocale.click();}
    public void setCreatedOn(){createdOn.click();}
    public void setLastCreatedOn(){lastCreatedOn.click();}



}

package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.Accomodation;
import util.DriverManager;

/**
 * Created by naveen on 4/4/17.
 */
public class AccomodationSteps {
    public AccomodationSteps(){

    }
    public Accomodation Login(String nam,String amt){
        Accomodation accomodationpgobj = PageFactory.initElements(DriverManager.driver,Accomodation.class);
        accomodationpgobj.setGlobalInterface();
        accomodationpgobj.setAccomodation();
        accomodationpgobj.setAddUser();
        accomodationpgobj.setName(nam);
        accomodationpgobj.setAmount(amt);
        accomodationpgobj.setSubmitButton();
        return accomodationpgobj;
        }
}

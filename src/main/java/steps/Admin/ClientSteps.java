package steps.Admin;

import org.openqa.selenium.support.PageFactory;
import pageobject.Admin.Clients;
import util.DriverManager;

/**
 * Created by naveen on 7/4/17.
 */
public class ClientSteps {
    public ClientSteps() {

    }

    public Clients Login(String nam, String l, String a, String s, String lat, String lon) {
        Clients clipgobj = PageFactory.initElements(DriverManager.driver, Clients.class);
        clipgobj.clickOnAdmin();
        clipgobj.clickOnClients();
        clipgobj.clickOnAddClient();
        clipgobj.clickOnName(nam);
        clipgobj.select_List(l);
        clipgobj.enterAddress(a);
        clipgobj.select_status(s);
        clipgobj.enter_Lat(lat);
        clipgobj.enter_long(lon);
        clipgobj.clickOnSubmit();
        return clipgobj;


    }
}

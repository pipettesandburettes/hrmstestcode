package steps.Admin;

import org.openqa.selenium.support.PageFactory;
import pageobject.Admin.Groups;
import util.DriverManager;

/**
 * Created by naveen on 7/4/17.
 */
public class GroupSteps {
    public  GroupSteps(){}

    public Groups Login(String nam,String cli,String sh){
        Groups grppgobj = PageFactory.initElements(DriverManager.driver,Groups.class);
        grppgobj.setAdmin();
        grppgobj.setGroups();
        grppgobj.setAddGroups();
        grppgobj.setName(nam);
        grppgobj.setClient(cli);
        grppgobj.setShared(sh);
        return grppgobj;
    }
}

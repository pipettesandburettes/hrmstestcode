package steps.Admin;

import org.openqa.selenium.support.PageFactory;
import pageobject.Admin.Roles;
import util.DriverManager;

/**
 * Created by naveen on 7/4/17.
 */
public class RoleSteps {
    public RoleSteps(){}
    public Roles Login(String nam,String stat){
        Roles rolepgobj = PageFactory.initElements(DriverManager.driver,Roles.class);
        rolepgobj.clickOnAdmin();
        rolepgobj.setClickOnrole();
        rolepgobj.clickOnRole();
        rolepgobj.enterName(nam);
        rolepgobj.select_status(stat);
        rolepgobj.setClickOnButton();

        return rolepgobj;
    }
}

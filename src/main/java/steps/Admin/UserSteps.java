package steps.Admin;

import org.openqa.selenium.support.PageFactory;
import pageobject.Admin.User;
import util.DriverManager;

/**
 * Created by naveen on 7/4/17.
 */
public class UserSteps{
    User userpgobj;


    public UserSteps(){

        userpgobj = PageFactory.initElements(DriverManager.driver,User.class);
    }
    public void admin() {

        userpgobj.clickOnAdmin();
        userpgobj.setClickOnUser();
        userpgobj.setAddUser();

    }
    public void user(String unam,String pass, String nam, String nick,String stat,String cat,String emai) throws InterruptedException {
        userpgobj.setUsername(unam);
        userpgobj.setAvalilable();
        userpgobj.setPassword(pass);
        userpgobj.setName(nam);
        userpgobj.setNickname(nick);
        userpgobj.select_status(stat);
        userpgobj.setEmployeename(cat);
        userpgobj.setEmail(emai);
        userpgobj.setSubmit();

    }


}

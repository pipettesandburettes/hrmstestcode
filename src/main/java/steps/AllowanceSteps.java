package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.AllowanceType;
import util.DriverManager;

/**
 * Created by naveen on 4/4/17.
 */
public class AllowanceSteps {
    AllowanceType allowancepgobj;

    public AllowanceSteps(){

        allowancepgobj= PageFactory.initElements(DriverManager.driver,AllowanceType.class);

    }

    public void setGlobal() {

        allowancepgobj.setGlobalInterface();
        allowancepgobj.setAllowancetype();
        allowancepgobj.setAddAllowancetype();

    }
    public void setAllowancepgobj(String nam, String naml,String cli) {
        allowancepgobj.setName(nam);
        allowancepgobj.setNamLocal(naml);
        allowancepgobj.setClientid(cli);
        allowancepgobj.setSubmitButton();


    }
}

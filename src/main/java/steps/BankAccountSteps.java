package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.BankAccount;
import util.DriverManager;

/**
 * Created by naveen on 3/4/17.
 */
public class BankAccountSteps {
    BankAccount bankAccount;



    public BankAccountSteps(){

        bankAccount = PageFactory.initElements(DriverManager.driver, BankAccount.class);
    }
    public  void globalInterfacePage(){
        bankAccount.setGlobalInterface();
        bankAccount.setBankAccount();
        bankAccount.setAddBankAccount();
    }


    public void AddBankAccount(String bankname,String namloc, String cli,String branchNam, String add, String ifsc, String phone, String stat, String accno, String iban){


        bankAccount.setBankName(bankname);
        bankAccount.setNamelocal(namloc);
        bankAccount.setClientid(cli);
        bankAccount.setBranchName(branchNam);
        bankAccount.setAddress(add);
        bankAccount.setIfscCode(ifsc);
        bankAccount.setPhoneNo(phone);
        bankAccount.select_status(stat);
        bankAccount.setAccountNo(accno);
        bankAccount.setIbanNo(iban);
        bankAccount.setSubmitButton();

    }
}

package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.Company;
import util.DriverManager;

/**
 * Created by naveen on 3/4/17.
 */
public class CompanySteps {

    Company comppgobj;

    public CompanySteps() {

        comppgobj = PageFactory.initElements(DriverManager.driver, Company.class);

    }

    public void setGlobalInterface() {

        comppgobj.setGlobalInterface();
        comppgobj.setCompany();
        comppgobj.setAddUser();

    }
public void setCompanyName(String nam, String cli, String loc, String add){

    comppgobj.setName(nam);
    comppgobj.setClientid(cli);
    comppgobj.setNamLocal(loc);
    comppgobj.setAddress(add);
    comppgobj.setSubmitButton();

}
}




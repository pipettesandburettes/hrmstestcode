package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.CostCenter;
import util.DriverManager;

/**
 * Created by naveen on 3/4/17.
 */
public class CostCenterSteps {

    CostCenter costCenter;
    public CostCenterSteps(){

        costCenter =PageFactory.initElements(DriverManager.driver,CostCenter.class);

    }
    public void setGlobal(){
        costCenter.setGlobalInterface();
        costCenter.setCostCenter();
        costCenter.setAddUser();
    }
    public void setcostName(String nam,String naml,String cli){
        costCenter.setName(nam);
        costCenter.setNamLocal(naml);
        costCenter.setClientid(cli);
        costCenter.setSubmitButton();
    }


}

package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.Department;
import util.DriverManager;

/**
 * Created by naveen on 4/4/17.
 */
public class DepartmentSteps {
    public DepartmentSteps(){

    }
    public Department Login(String nam,String dept){
        Department deptpgobj = PageFactory.initElements(DriverManager.driver,Department.class);
        deptpgobj.setGlobalInterface();
        deptpgobj.setDepartment();
        deptpgobj.setAddUser();
        deptpgobj.setName(nam);
        deptpgobj.setParent_department(dept);
        deptpgobj.setSubmitButton();
        return deptpgobj;
    }

}

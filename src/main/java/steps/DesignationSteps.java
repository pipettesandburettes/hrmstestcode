package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.Designation;
import util.DriverManager;

/**
 * Created by naveen on 3/4/17.
 */
public class DesignationSteps {
    Designation desigpgobj;

    public DesignationSteps() {
        desigpgobj = PageFactory.initElements(DriverManager.driver, Designation.class);
    }
    public void setGlobalInterface() {

        desigpgobj.setGlobalInter();
        desigpgobj.setDesignation();
    }
    public void setDesignation(String nam,String naml,String cli,String job,String par,String lev){

        desigpgobj.setAddUser();
        desigpgobj.setName(nam);
        desigpgobj.setName_local(naml);
        desigpgobj.setClientid(cli);
        desigpgobj.setJobDescription(job);
        desigpgobj.setParentdepartment(par);

        desigpgobj.setLevel(lev);
        desigpgobj.setSubmitButton();

    }
}

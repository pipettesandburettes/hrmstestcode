package steps.Edit;

import org.openqa.selenium.support.PageFactory;
import pageobject.Edit.CompanyEditPage;
import util.DriverManager;

/**
 * Created by naveen on 17/4/17.
 */
public class CompanyEditSteps {
    CompanyEditPage comppgobj;
    boolean clearCurrentValue = true;
    public  CompanyEditSteps(){
        comppgobj = PageFactory.initElements(DriverManager.driver,CompanyEditPage.class);
    }

    public void pageRedirection(){
        comppgobj.setGlobalInterface();
        comppgobj.setCompany();
    }


   /* public void clickOnEdit(WebElement btnEle, WebElement editEle){
        comppgobj.setClickonButton(btnEle);
        comppgobj.setClickonEdit(editEle);
    }*/


    public CompanyEditPage setFields(String nam,String add, String loc){

        comppgobj.setName(nam, !clearCurrentValue);
        comppgobj.setAddress(add, !clearCurrentValue);
        comppgobj.setNamLocal(loc, !clearCurrentValue);
        comppgobj.setSubmitButton();
        return comppgobj;
    }

    public void resetFields(String nam,String add, String loc){

        String name = comppgobj.getName();
        String addr = comppgobj.getAddress();
        String nameLocale = comppgobj.getNamLocal();

        name = name.replace(nam,"");
        addr = addr.replace(add,"");
        nameLocale = nameLocale.replace(loc,"");

        /*if(addr.isEmpty())
            addr = "Addr";*/

        comppgobj.setName(name, clearCurrentValue);
        comppgobj.setAddress(addr, clearCurrentValue);
        comppgobj.setNamLocal(nameLocale,clearCurrentValue);
        comppgobj.setSubmitButton();

    }

}




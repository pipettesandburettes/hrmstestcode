package steps.EmployeeSelfService;

import org.openqa.selenium.support.PageFactory;
import pageobject.EmployeeSelfService.AddGrievance;
import util.DriverManager;

/**
 * Created by naveen on 5/4/17.
 */
public class AddGrievanceSteps {
    public AddGrievanceSteps(){

    }
    public AddGrievance Login(String empid,String gretype,String rep,String des){
        AddGrievance addgrievance = PageFactory.initElements(DriverManager.driver,AddGrievance.class);
        addgrievance.setEmployee();
        addgrievance.setSelfService();
        addgrievance.setAddGrievance();
        addgrievance.setEmpId(empid);
        addgrievance.setGrievanceType(gretype);
        addgrievance.setReportDate(rep);
        addgrievance.setDescription(des);
        addgrievance.setSubmitButton();

        return addgrievance;
    }

}

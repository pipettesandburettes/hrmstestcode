package steps.EmployeeSelfService;

import org.openqa.selenium.support.PageFactory;
import pageobject.EmployeeSelfService.ApplyforLeave;
import util.DriverManager;

/**
 * Created by naveen on 5/4/17.
 */
public class ApplyforLeaveSteps {

    ApplyforLeave applyforLeave;

    public ApplyforLeaveSteps(){
        applyforLeave = PageFactory.initElements(DriverManager.driver,ApplyforLeave.class);

    }
    public void setEmployeeSErvie(){
        applyforLeave.setEmployee();
        applyforLeave.setSelfService();
        applyforLeave.setApplyforLeave();
    }

    public void setLeaveApply(String leav,String rea,String rem){

        applyforLeave.setAddleave();
        applyforLeave.setLeaveTypeId(leav);
        applyforLeave.setFromDate();
        applyforLeave.setToDate();
        applyforLeave.setReason(rea);
        applyforLeave.setRemarks(rem);
        applyforLeave.setSubmitButton();

    }
    public void setLogout(){
        applyforLeave.setCliekOnUsername();
        applyforLeave.setClickOnLogout();
    }




}

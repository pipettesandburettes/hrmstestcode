package steps.GlobalInterface;

import org.openqa.selenium.support.PageFactory;
import pageobject.GlobalInterface.Holiday;
import util.DriverManager;

/**
 * Created by naveen on 5/5/17.
 */
public class HolidaySteps {

    Holiday holiday;
    public HolidaySteps(){
    holiday = PageFactory.initElements(DriverManager.driver,Holiday.class);
    }

    public void setGlobal(){
    holiday.setGlobalInterface();
        holiday.setHoliday();
        holiday.setAddHoliday();
    }
    public void setHolidayData(String cli,String nam, String rem){
    holiday.setClientid(cli);
        holiday.setHol_date();
        holiday.setName(nam);
        holiday.setRemark(rem);
        holiday.setSubmitButton();

    }


}

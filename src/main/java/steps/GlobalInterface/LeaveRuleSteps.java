package steps.GlobalInterface;

import org.openqa.selenium.support.PageFactory;
import pageobject.GlobalInterface.LeaveRule;
import util.DriverManager;

/**
 * Created by naveen on 5/5/17.
 */
public class LeaveRuleSteps {

    LeaveRule leaveRule;

    public LeaveRuleSteps(){
    leaveRule = PageFactory.initElements(DriverManager.driver,LeaveRule.class);
    }

    public void setGlobal(){
        leaveRule.setGlobalInterface();
        leaveRule.setLeaveRule();
        leaveRule.setAddLeaveRule();
    }
    public void setLeaveRuleData(String a,String b, String c,String d,String e,String f){



        leaveRule.setLeavetypeid(a);
        leaveRule.setValidfromdate();
        leaveRule.setValidtilldate();
        leaveRule.setLeavecreditcount(b);
        leaveRule.setLeaveCreditOn(c);
        leaveRule.setLeaveMaturedOn(d);
        leaveRule.setClubbedWithCl(e);
        leaveRule.setClubbedWithHoliday(f);
        leaveRule.setSubmitButton();
    }




}

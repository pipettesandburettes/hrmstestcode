package steps.GlobalInterface;

import org.openqa.selenium.support.PageFactory;
import pageobject.GlobalInterface.LeaveType;
import util.DriverManager;

/**
 * Created by naveen on 5/5/17.
 */
public class LeaveTypeSteps {

    LeaveType leaveType;

    public LeaveTypeSteps(){
leaveType = PageFactory.initElements(DriverManager.driver,LeaveType.class);


    }

    public void setGlobal(){
        leaveType.setGlobalInterface();
        leaveType.setLeavetype();
        leaveType.setAddleave();

    }
    public void setLeaveData(String cli, String lea, String nam, String car){
        leaveType.setClientid(cli);
        leaveType.setLeave(lea);
        leaveType.setLeaveName(nam);
        leaveType.setCarryForward(car);
        leaveType.setSubmitButton();
    }

}

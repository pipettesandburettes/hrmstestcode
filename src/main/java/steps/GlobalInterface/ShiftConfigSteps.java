package steps.GlobalInterface;

import org.openqa.selenium.support.PageFactory;
import pageobject.GlobalInterface.ShiftConfiguration;
import util.DriverManager;

/**
 * Created by naveen on 11/5/17.
 */
public class ShiftConfigSteps {

    ShiftConfiguration shiftConfiguration;

    public ShiftConfigSteps(){
        shiftConfiguration = PageFactory.initElements(DriverManager.driver,ShiftConfiguration.class);

    }

    public void setGlobalInterface(){
        shiftConfiguration.setGlobalInterface();
        shiftConfiguration.setShiftConfiguration();
        shiftConfiguration.setAddShiftConfig();
    }
    public void setShiftConfiguration(String  nam,String naml,String cli,String exe, String min){
        shiftConfiguration.setShiftName(nam);
        shiftConfiguration.setNameLOcal(naml);
        shiftConfiguration.setClientid(cli);
        shiftConfiguration.setStartTime();
        shiftConfiguration.setBreakTime();
        shiftConfiguration.setBreakEnd();
        shiftConfiguration.setEndTime();
        shiftConfiguration.setExceptionTime(exe);
        shiftConfiguration.setMinWorkingHours(min);
        shiftConfiguration.setSubmitButton();
    }



}

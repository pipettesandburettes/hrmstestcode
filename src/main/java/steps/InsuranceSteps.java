package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.InsuranceTYpe;
import util.DriverManager;

/**
 * Created by naveen on 3/4/17.
 */
public class InsuranceSteps {
    public InsuranceSteps(){

    }
    public InsuranceTYpe Login(String nam){
        InsuranceTYpe isurancepgobj = PageFactory.initElements(DriverManager.driver,InsuranceTYpe.class);
        isurancepgobj.setGlobalInterface();
        isurancepgobj.setInsuranceType();
        isurancepgobj.setAddUser();
        isurancepgobj.setName(nam);
        isurancepgobj.setSubmitButton();
        return isurancepgobj;
    }
}

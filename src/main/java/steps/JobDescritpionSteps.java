package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.JobDescription;
import util.DriverManager;

/**
 * Created by naveen on 4/4/17.
 */
public class JobDescritpionSteps {
    JobDescription jobpgobj ;



    public JobDescritpionSteps(){
         jobpgobj = PageFactory.initElements(DriverManager.driver,JobDescription.class);
    }
    public void setGlobal() {

        jobpgobj.setGlobalInterface();
        jobpgobj.setJobDescription();
        jobpgobj.setAddUser();

    }
    public void setJobpgobj(String nam, String naml,String cli) {
        jobpgobj.setName(nam);
        jobpgobj.setNamLocal(naml);
        jobpgobj.setClientid(cli);
        jobpgobj.setSubmitButton();


    }
}

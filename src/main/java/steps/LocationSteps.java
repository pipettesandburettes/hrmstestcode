package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.Location;
import util.DriverManager;

/**
 * Created by naveen on 4/4/17.
 */
public class LocationSteps {

public LocationSteps(){

}
public Location Login(String nam) {
    Location locpgobj = PageFactory.initElements(DriverManager.driver, Location.class);
    locpgobj.setName(nam);
    return locpgobj;
}
}

package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.LoginPage;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 3/4/17.
 */
public class LoginSteps {
    //initialize the elemnts using page factory
    public LoginSteps() throws IOException {
    }

    public LoginPage Login(String uname, String pwd) {


        LoginPage loginPageObj = PageFactory.initElements(DriverManager.driver, LoginPage.class);

        loginPageObj.userLogin(uname, pwd);

        return loginPageObj;
    }
}




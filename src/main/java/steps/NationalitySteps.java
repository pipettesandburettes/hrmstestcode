package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.Nationality;
import util.DriverManager;

/**
 * Created by naveen on 4/4/17.
 */
public class NationalitySteps {

    public NationalitySteps(){

    }
    public Nationality Login(String nam){
        Nationality loginpgobj =  PageFactory.initElements(DriverManager.driver, Nationality.class);
        loginpgobj.setName(nam);
        return loginpgobj;
    }

}

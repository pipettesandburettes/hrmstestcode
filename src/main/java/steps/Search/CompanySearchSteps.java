package steps.Search;

import org.openqa.selenium.support.PageFactory;
import pageobject.Search.CompanySearchPage;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 19/4/17.
 */
public class CompanySearchSteps {
    CompanySearchPage loginPageObj;
    public CompanySearchSteps() throws IOException {
        loginPageObj = PageFactory.initElements(DriverManager.driver, CompanySearchPage.class);
    }

    public void setTargetPage() {

        loginPageObj.setGlobalInterface();
        loginPageObj.setCompany();

    }

    public void searchAction(String nam, String add) {
        loginPageObj.setName(nam);
        loginPageObj.setAddress(add);
        loginPageObj.setSearch();
    }


}

package steps;

import org.openqa.selenium.support.PageFactory;
import pageobject.Section;
import util.DriverManager;

/**
 * Created by naveen on 4/4/17.
 */
public class SectionSteps {
    public SectionSteps(){

    }
    public Section Login(String nam){
        Section sectionpgobj= PageFactory.initElements(DriverManager.driver,Section.class);
        sectionpgobj.setGlobalInterface();
        sectionpgobj.setSection();
        sectionpgobj.setAddUser();
        sectionpgobj.setName(nam);
        sectionpgobj.setSubmitButton();

        return sectionpgobj;
    }


}

package steps.SmartDocs;

import SmartDocs.Documents;
import org.openqa.selenium.support.PageFactory;
import util.DriverManager;

/**
 * Created by naveen on 4/4/17.
 */
public class DocumentSteps {
    Documents document;


    public DocumentSteps() {
    document= PageFactory.initElements(DriverManager.driver,Documents.class);
    }
    public void setSmartDoc(){
        document.setSmartDocs();
        document.setDocuments();
        document.setAddDocuments();
    }

    public void sDocumentNames(String nam,String fold,String cli, String sh,String lg,String shar,String per, String tg,String mt,String lk,String up) throws Exception
    {
        document.setName(nam);
        document.setExpiryDate();
        document.setFolderid(fold);
        document.setClientid(cli);
        document.setShortDesc(sh);
        document.setLongDesc(lg);
        document.setShared(shar);
        document.setPermission(per);
        document.setTags(tg);
        document.setMetaTags(mt);
        document.setLinkedDocId(lk);
        document.setUploadFile(up);
        document.setSubmitButton();
    }

}


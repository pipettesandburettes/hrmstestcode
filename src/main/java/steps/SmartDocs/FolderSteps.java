package steps.SmartDocs;

import SmartDocs.Folder;
import org.openqa.selenium.support.PageFactory;
import util.DriverManager;

/**
 * Created by naveen on 17/5/17.
 */
public class FolderSteps {

    Folder folder;

    public FolderSteps(){

        folder= PageFactory.initElements(DriverManager.driver, Folder.class);

    }
    public void setSmart(){
        folder.setSmartdocs();
        folder.setFolder();
        folder.setAddFolder();
    }

    public void setAddFolderName(String nam,String parid,String cli,String sh,String per) throws InterruptedException {
       folder.setName(nam);
       folder.setParentId(parid);
       folder.setClientid(cli);
       folder.setShared(sh);
        try {
            folder.setPermission(per);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        folder.setSubmitButton();

    }






}

package steps.Sorting;

import org.openqa.selenium.support.PageFactory;
import pageobject.Sorting.CompanySortPage;
import util.DriverManager;

/**
 * Created by naveen on 18/4/17.
 */
public class CompanySortSteps {

    CompanySortPage comppgobj;

    public CompanySortSteps() {
        comppgobj = PageFactory.initElements(DriverManager.driver, CompanySortPage.class);
    }

    public void companySort() {

        comppgobj.setGlobalInterface();
        comppgobj.setCompany();

    }
    public  void setCompanySort(){


        comppgobj.setCompanyName();
//        comppgobj.setAddress();
//        comppgobj.setNameLocale();
//        comppgobj.setCreatedOn();
//        comppgobj.setLastCreatedOn();

    }
}

package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by naveen on 6/3/17.
 */
public class DriverManager
{
    public static WebDriver driver;
    String baseUrl;
    public Properties prop;

    public DriverManager() throws IOException {
        // Get baseUrl property
        File file = new File("config.properties");
        FileInputStream fileInput = null;
        try {
            fileInput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //load properties file
        prop = new Properties();
        try {
            prop.load(fileInput);
        } catch (IOException e) {
            e.printStackTrace();
        }



        // Start driver
        System.setProperty("webdriver.chrome.driver", "/home/naveen/chromedriver");
        driver = new ChromeDriver();

        driver.manage().window().maximize();
        baseUrl = prop.getProperty("baseurl");
        driver.get(baseUrl);

    }
}


package Admin;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.Admin.UserSteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 4/5/17.
 */
public class Users extends DriverManager {

    UserSteps user;
    public Users()  throws Exception
    {

        user =new UserSteps();
    }

    @BeforeSuite
    public void initDRiver() throws IOException {
        LoginPage page=new LoginSteps().Login(prop.getProperty("n"),prop.getProperty("p"));
        user.admin();
    }
    @Test(dataProvider ="SearchProvider")
    public void addUser(String unam,String pass, String nam, String nick,String stat,String cat,String emai) throws InterruptedException {
        try {
            user.user(unam,pass,nam,nick,stat,cat,emai);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(driver.getPageSource().contains("List Users"));


    driver.findElement(By.xpath("//*[@id=\'search-container\']/a/i")).click();
    }





@DataProvider(name ="SearchProvider")
public static Object[][] getDataFromDataprovider() {
    return new Object[][]{
            {"neetha123","neetha123","neetha vj","Nita","Employee [ThoughtWorks]","Anver","neetha@thinkberries.com"},
            {"naveen123","naveen123","naveen","naveen","Employee [ThoughtWorks]","champu","naveend@thinkberries.com"},
            {"cerin123","cerin123","cerin","cerin","Employee [ThoughtWorks]","basil","cerin@thinkberries.com"}
    };
}}


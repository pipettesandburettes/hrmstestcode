package Edit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.Edit.CompanyEditSteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;
import java.util.List;

/**
 * Created by naveen on 17/4/17.
 */
public class EditCompany extends DriverManager{

    CompanyEditSteps comsteps;

    public EditCompany() throws IOException {



            comsteps =new CompanyEditSteps();
        }

        @BeforeSuite
        public void initDRiver() throws IOException{
            LoginPage page=new LoginSteps().Login(prop.getProperty("n"),prop.getProperty("p"));
        }

        @Test(dataProvider ="SearchProvider")
        public void init(String nam,String add,String loc) {

            comsteps.pageRedirection();


            if (driver.getPageSource().contains("List Company Details")) {

                    List<WebElement> rows = driver.findElements(By.className("li-row"));
//                List<WebElement> links = rows.get(0).findElements(By.tagName("li"));
                // print the total number of elements

                int initialValue = 2, finalValue = rows.size()+2;
                for (int i = initialValue; i < finalValue; i++) {
                    /*try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }*/

                   driver.findElement(By.xpath("//*[@id=\'list-container\']/ul/li["+i+"]/span[1]/a/i")).click();
                    driver.findElement(By.linkText("Edit")).click();

                    // Setting patterns for editing
//                    comsteps.setFields(nam, add, loc);

                    // Reset all edited values
                    comsteps.resetFields(nam, add, loc);


                    String editedValue = driver.findElement(By.xpath("//*[@id=\'list-container\']/ul/li["+i+"]/span[3]")).getText();

//                        Assert.assertTrue(editedValue.contains(nam));

                        Assert.assertFalse(editedValue.contains(nam));


                }



            }
            }



    @DataProvider(name="SearchProvider")
    public  static Object[][]  getDataFromDataprovider(){
        return new Object[][] {
                { "_compNameEDT","_compADDEDT","_comp-CNT-EDT"}

        };
    }}






package Edit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.Edit.CompanyEditSteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;
import java.util.List;

/**
 * Created by naveen on 17/4/17.
 */
public class EditCompany2 extends DriverManager{

    CompanyEditSteps comsteps;

    public EditCompany2() throws IOException {



        comsteps =new CompanyEditSteps();
    }

    @BeforeSuite
    public void initDRiver() throws IOException{
        LoginPage page=new LoginSteps().Login(prop.getProperty("n"),prop.getProperty("p"));
    }

    @Test(dataProvider ="SearchProvider")
    public void init(String nam,String add,String loc) {
        comsteps.pageRedirection();
        comsteps.resetFields(nam, add, loc);


        if (driver.getPageSource().contains("List Company Details")) {


            List<WebElement> rows = driver.findElements(By.xpath("//*[@id=\"list-container\"]/ul"));

            // print the total number of elements
            System.out.println("Total selected rows are " + rows.size());

            for (int j = 0; j < rows.size(); j++) {

                WebElement box = rows.get(j);
                List<WebElement> links = box.findElements(By.tagName("li"));
                System.out.println("Total links for---" + (j + 1) + "---are--- " + links.size());

                for (int i = 1; i < links.size(); i++) {
                    System.out.println("*********************************************");
                    System.out.println(links.get(i).getText());


                }


            }
        }
    }

    @DataProvider(name="SearchProvider")
    public  static Object[][]  getDataFromDataprovider(){
        return new Object[][] {
                { "Apple Computers","India","Countery"}

        };
    }}






package EmployeeSelfServiceTest;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.EmployeeSelfService.ApplyforLeaveSteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by naveen on 5/4/17.
 */
public class ApplyforLeaveTest extends DriverManager
{
    ApplyforLeaveSteps applyforLeaveSteps;

    public ApplyforLeaveTest() throws Exception{

        applyforLeaveSteps=new ApplyforLeaveSteps();
    }


    @BeforeSuite
    public void initDRiver() throws IOException {
        LoginPage page=new LoginSteps().Login(prop.getProperty("empuser"),prop.getProperty("emppas"));
        applyforLeaveSteps.setEmployeeSErvie();
    }



    @AfterSuite
    public void managerLogin() throws IOException {
        applyforLeaveSteps.setLogout();
        LoginPage page=new LoginSteps().Login(prop.getProperty("manguser"),prop.getProperty("mangpas"));
        applyforLeaveSteps.setEmployeeSErvie();



    }

    @Test
    public void setManagerLoginTest(){


        Assert.assertTrue(driver.getPageSource().contains(""));
    }

    @Test(dataProvider = "SearchProvider")
    public void addComapnyNmae(String leav,String rea,String rem) throws InterruptedException {

       applyforLeaveSteps.setLeaveApply(leav, rea, rem);


       // Assert.assertTrue(driver.getPageSource().contains("List Leave"));

        for (int i = 0; i < 2; i++) {


            driver.findElement(By.linkText("Applied On")).click();
            Thread.sleep(1000);


        }

        String nam=driver.findElement(By.xpath("//*[@id='list-container']/div[1]/ul/li[2]/span[5]")).getText();

        String act=driver.findElement(By.xpath("//*[@id='list-container']/div[1]/ul/li[2]/span[10]")).getText();

        String abc=driver.findElement(By.xpath("//*[@id=\'list-container\']/div[1]/ul/li[2]/span[9]")).getText();
        abc= abc.replace("'"," ");
        System.out.println("Employe name :"+nam+"applied on :"+abc+ "action  :"+act);

        SimpleDateFormat format = new SimpleDateFormat("d");
        String date = format.format(new Date());

        if(date.endsWith("1") && !date.endsWith("11"))
            format = new SimpleDateFormat("d'st' MMM yy hh:mm:ss a");
        else if(date.endsWith("2") && !date.endsWith("12"))
            format = new SimpleDateFormat("d'nd' MMM yy hh:mm:ss a");
        else if(date.endsWith("3") && !date.endsWith("13"))
            format = new SimpleDateFormat("d'rd' MMM yy hh:mm:ss a");
        else
            format = new SimpleDateFormat("d'th' MMM yy hh:mm:ss a");

        String yourDate = format.format(new Date());
        System.out.println(">>: "+yourDate);


        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(abc);
            d2 = format.parse(yourDate);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();
            System.out.println(diff);
        }
        catch(Exception e){
            e.printStackTrace();
        }







    }

    @DataProvider(name = "SearchProvider")
    public static Object[][] getDataFromDataprovider() {
        return new Object[][]{


                {"office meeting leave - 15 left","Client meeting","Going to India"},





        };
    }}






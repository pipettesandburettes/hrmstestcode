package GlobalInterfaceTest;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.BankAccountSteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 5/4/17.
 */
public class BankAccountTest extends DriverManager {
    BankAccountSteps bankAccountSteps;


    public BankAccountTest() throws IOException {
        bankAccountSteps = new BankAccountSteps();
    }

    @BeforeSuite
    public void initDriver() throws IOException {
        LoginPage page = new LoginSteps().Login(prop.getProperty("n"), prop.getProperty("p"));
        bankAccountSteps.globalInterfacePage();
    }

    @Test(dataProvider = "SearchProvider")
    public void addBankAccountTest(String bankname, String namloc,String cli, String branchNam, String add, String ifsc, String phone, String stat, String accno, String iban) {

        bankAccountSteps.AddBankAccount(bankname, namloc,cli, branchNam, add, ifsc, phone, stat, accno, iban);


            Assert.assertTrue(driver.getPageSource().contains("List Bank Details"));


            driver.findElement(By.xpath("//*[@id=\"search-container\"]/a/i")).click();











    }



    @DataProvider(name = "SearchProvider")
    public static Object[][] getDataFromDataprovider() {
        return new Object[][]{
                //{"pUNJAB national  bank","الجنوب، الهند، بانك","Demo" ,"Barmuda","Manali","4562ERER122","9400693133","current","SDSAD5567890RFDSRF00","546574674sdfsaf"},
                {"Axis bank","الجنوب، الهند، بانك","ThoughtWorks" ,"Nepal","KASMIR","4562s343EREd","9946594830","current","GFGDFFG789ASDRASD","546574674swewewerw"},

                {"Indian bank","الجنوب، الهند، بانك","ThoughtWorks" ,"pakistan","gURUVAEER","456WEWE434","1234567890","current","54DFASDFSA242rewrer","546574674swewew424w"},
                {"Chennai bank","الجنوب، الهند، بانك","ThoughtWorks" ,"Japan","aNTARTICS","45623SDFSFd","0123456789","savings","ASDASDSA4543490srewrer","546574674swewewerw"},


                {"SCotland bank","الجنوب، الهند، بانك","ThoughtWorks" ,"China","aTLANTA","4562sDSDADFS","1122334455","current","FFDASDFDFASDFASD743430srewrer","54657467343wewewerw"}

        };
    }}

package GlobalInterfaceTest;

import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.CompanySteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 4/5/17.
 */
public class Company extends DriverManager {

   CompanySteps companySteps;

    public Company() throws  Exception
    {

        companySteps=new CompanySteps();

    }

    @BeforeSuite
    public void initDriver() throws IOException {
        LoginPage page = new LoginSteps().Login(prop.getProperty("n"), prop.getProperty("p"));
        companySteps.setGlobalInterface();
    }

    @Test(dataProvider = "SearchProvider")
    public void addComapnyNmae(String nam, String cli, String loc, String add) {

       companySteps.setCompanyName(nam,cli,loc,add);


        Assert.assertTrue(driver.getPageSource().contains("List Company Details"));



    }



    @DataProvider(name = "SearchProvider")
    public static Object[][] getDataFromDataprovider() {
        return new Object[][]{


                {"MUMBAI iNDIANS","Demo","ddfd","Kochi"}




        };
    }}

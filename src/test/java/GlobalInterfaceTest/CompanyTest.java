//package GlobalInterfaceTest;
//
//import org.testng.Assert;
//import org.testng.annotations.BeforeSuite;
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Test;
//import pageobject.LoginPage;
//import steps.CompanySortSteps;
//import steps.LoginSteps;
//import util.DriverManager;
//
//import java.io.IOException;
//
///**
// * Created by naveen on 5/4/17.
// */
//public class CompanyTest extends DriverManager{
//
//    CompanySortSteps comsteps;
//    int inputDataFlag=0;
//    public CompanyTest() throws IOException {
//    comsteps =new CompanySortSteps();
//    }
//
//    @BeforeSuite
//    public void initDRiver() throws IOException{
//        LoginPage page=new LoginSteps().Login(prop.getProperty("n"),prop.getProperty("p"));
//    }
//
//    @Test(dataProvider = "setData")
//    public void init(String nam,String add) {
//
//        comsteps.Login(nam,add);
//        Assert.assertFalse(driver.getPageSource().contains("List Company Details"));
//
//    }
//    @Test(priority = 1, dataProvider = "setData")
//    public void ini(String nam,String add) {
//        comsteps.Login(nam,add);
//        Assert.assertTrue(driver.getPageSource().contains("List Company Details"));
//    }
//
//    @DataProvider
//    Object[][] setData() {
//        Object[][] dataValues = getValues();
//        return dataValues;
//
//    }
//
//    private String[][] getValues() {
//
//        String[] namArray = new String[0];
//        String[] addArray = new String[0];
//
//
//        if (inputDataFlag == 0) {
//            namArray = prop.getProperty("invalidnam").split(",");
//            addArray = prop.getProperty("invalidadd").split(",");
//
//
//        } else if (inputDataFlag == 1) {
//
//
//            namArray = prop.getProperty("validnam").split(",");
//            addArray = prop.getProperty("validadd").split(",");
//
//        }
//
//
//        String nam, add;
//
//        int len = namArray.length;
//
//        String[][] dataValues = new String[len][2];
//
//        for (
//                int i = 0; i < len;i++)
//
//        {
//            nam = namArray[i];
//
//            add = addArray[i];
//
//
//            dataValues[i][0] = nam;
//
//            dataValues[i][1] = add;
//
//        }
//        inputDataFlag++;
//
//        return dataValues;
//
//    }
//
////    @AfterSuite
////   public void close(){driver.close();}
//}
//
//
//
//

package GlobalInterfaceTest;

import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.DepartmentSteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 5/4/17.
 */
public class DepartmentTest extends DriverManager {
    DepartmentSteps departsteps;
    int inputDataFlag=0;
    public DepartmentTest() throws IOException {
        departsteps = new DepartmentSteps();
    }

    @BeforeSuite
    public void initDRiver() throws IOException {
        LoginPage page = new LoginSteps().Login(prop.getProperty("n"), prop.getProperty("p"));
    }

    @Test(dataProvider = "setData")
    public void init(String nam, String dept) {
        departsteps.Login(nam, dept);
        Assert.assertFalse(driver.getPageSource().contains("List of Departments"));

    }

    @Test(priority = 1, dataProvider = "setData")
    public void ini(String nam, String dept) {
        departsteps.Login(nam, dept);
        Assert.assertTrue(driver.getPageSource().contains("List of Departments"));
    }

    @DataProvider
    Object[][] setData() {

        Object[][] datavalues = getValues();
        return datavalues;
    }

    private String[][] getValues() {

        String[] namArray = new String[0];
        String[] deptArray = new String[0];


        if (inputDataFlag == 0) {
            namArray = prop.getProperty("invalidn").split(",");
            deptArray = prop.getProperty("invaliddept").split(",");


        } else if (inputDataFlag == 1) {


            namArray = prop.getProperty("validn").split(",");
            deptArray = prop.getProperty("validdept").split(",");

        }


        String nam, dept;

        int len = namArray.length;

        String[][] dataValues = new String[len][2];

        for (
                int i = 0; i < len;i++)

        {
            nam = namArray[i];

            dept = deptArray[i];


            dataValues[i][0] = nam;

            dataValues[i][1] = dept;

        }
        inputDataFlag++;

        return dataValues;

    }

//    @AfterSuite
//   public void close(){driver.close();}
}





package GlobalInterfaceTest;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.GlobalInterface.HolidaySteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 5/5/17.
 */
public class HolidayTest extends DriverManager {
    HolidaySteps holidaySteps;

    public HolidayTest() throws IOException {
        holidaySteps =new HolidaySteps();

    }


    @BeforeSuite
    public void initDriver() throws IOException {
        LoginPage page = new LoginSteps().Login(prop.getProperty("n"), prop.getProperty("p"));
        holidaySteps.setGlobal();
    }

    @Test(dataProvider = "SearchProvider")
    public void addComapnyNmae(String cli,String nam, String rem) {

        holidaySteps.setHolidayData(cli, nam, rem);


        Assert.assertTrue(driver.getPageSource().contains("List Holiday Configuration"));

        driver.findElement(By.xpath("//*[@id=\'search-container\']/a/i")).click();



    }






    @DataProvider(name = "SearchProvider")
    public static Object[][] getDataFromDataprovider() {
        return new Object[][]{


                {"ThoughtWorks","Independence Day","Indian Independence Day"},
                {"ThoughtWorks","Republic DAy","indian REpublic Day"},
                {"ThoughtWorks","May Day","May Day"}





        };
    }}



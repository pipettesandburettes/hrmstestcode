package GlobalInterfaceTest;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.JobDescritpionSteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 5/4/17.
 */
public class JobDescriptionTest extends DriverManager {

    JobDescritpionSteps jobDescritpionSteps;


    public JobDescriptionTest() throws IOException {

        jobDescritpionSteps =new JobDescritpionSteps();
    }


    @BeforeSuite
    public void initDriver() throws IOException {
        LoginPage page = new LoginSteps().Login(prop.getProperty("n"), prop.getProperty("p"));
        jobDescritpionSteps.setGlobal();
    }

    @Test(dataProvider = "SearchProvider")
    public void addComapnyNmae(String nam, String naml, String cli) throws InterruptedException {

        jobDescritpionSteps.setJobpgobj(nam, naml, cli);


        Assert.assertTrue(driver.getPageSource().contains("List Job Description"));



            if (driver.getPageSource().contains("List Job Description")) {
                driver.findElement(By.xpath("//*[@id=\'search-container\']/a/i")).click();
            }
        Thread.sleep(1000);
            String abc = driver.findElement(By.xpath("//*[@id=\'addjobdescription\']/div/form/div[1]/div/span")).getText();
            String def = driver.findElement(By.xpath("//*[@id=\'addjobdescription\']/div/form/div[3]/div/span")).getText();
        System.out.println(abc);
        System.out.println(def);

            if (abc == null) {
                Assert.assertEquals(abc,"Name is Mandatory!");
            }
            if (def.contains("None")) {
                Assert.assertTrue(driver.getPageSource().contains("Client is Mandatory!"));

            }
            if (abc == null && def.contains("None")) {
                Assert.assertEquals(def, "Client is Mandatory!");

            }

        }



    @DataProvider(name = "SearchProvider")
    public static Object[][] getDataFromDataprovider() {
        return new Object[][]{

                {"network Engineer","","Demo"},
                {"","","None"}




        };
    }}


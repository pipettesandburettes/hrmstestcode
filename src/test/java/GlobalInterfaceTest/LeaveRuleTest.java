package GlobalInterfaceTest;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.GlobalInterface.LeaveRuleSteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 5/5/17.
 */
public class LeaveRuleTest extends DriverManager {

    LeaveRuleSteps leaveRuleSteps;
    public LeaveRuleTest()  throws Exception
    {
    leaveRuleSteps =new LeaveRuleSteps();

    }

    @BeforeSuite
    public void initDriver() throws IOException {
        LoginPage page = new LoginSteps().Login(prop.getProperty("n"), prop.getProperty("p"));
        leaveRuleSteps.setGlobal();
    }

    @Test(dataProvider = "SearchProvider")
    public void addComapnyNmae(String a,String b, String c,String d,String e,String f){

        leaveRuleSteps.setLeaveRuleData(a,b,c,d,e,f);

        Assert.assertTrue(driver.getPageSource().contains("List Leave Rule Configuration"));

        if(driver.getPageSource().contains("List Leave Rule Configuration"))
        {
            driver.findElement(By.xpath("//*[@id='search-container']/a/i")).click();
        }

    }






    @DataProvider(name = "SearchProvider")
    public static Object[][] getDataFromDataprovider() {
        return new Object[][]{


                {"office meeting leave [ThoughtWorks]","15","Financial Year Begin","Financial Year End","Yes","Yes" },
                {"workshop leave [ThoughtWorks]","10","Financial Year Begin","Financial Year End","Yes","Yes"},
                {"Formal Leave [ThoughtWorks]","10","Financial Year Begin","Financial Year End","Yes","Yes"},
                {"Casual Leave [ThoughtWorks]","10","Financial Year Begin","Financial Year End","Yes","Yes"},



        };
    }}









package GlobalInterfaceTest;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.GlobalInterface.LeaveTypeSteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 5/5/17.
 */
public class LeaveTypeTest  extends DriverManager{

    LeaveTypeSteps leaveTypeSteps;

    public LeaveTypeTest() throws Exception
    {
    leaveTypeSteps =new LeaveTypeSteps();

    }
    @BeforeSuite
    public void initDriver() throws IOException {
        LoginPage page = new LoginSteps().Login(prop.getProperty("n"), prop.getProperty("p"));
        leaveTypeSteps.setGlobal();
    }

    @Test(dataProvider = "SearchProvider")
    public void addComapnyNmae(String cli,String lea,String nam,String car) {

        leaveTypeSteps.setLeaveData(cli,lea,nam,car);


        Assert.assertTrue(driver.getPageSource().contains("List Leave Type Configuration"));

        if(driver.getPageSource().contains("List Leave Type Configuration"))
        {
            driver.findElement(By.xpath("//*[@id=\'search-container\']/a/i")).click();
        }

    }






    @DataProvider(name = "SearchProvider")
    public static Object[][] getDataFromDataprovider() {
        return new Object[][]{


                {"ThoughtWorks","Casual Leave","Leave with salary","Yes"},
                {"ThoughtWorks","Hospital Leave","Leave without salary","Yes"},

                {"ThoughtWorks","Sad Leave","Leave with salary","No"},

                {"ThoughtWorks","Sick Leave","Leave with salary","Yes"},

                {"ThoughtWorks","Formal Leave","Leave with salary","No"},
                {"ThoughtWorks","office meeting leave ","Leave without salary","No"},
                {"ThoughtWorks","workshop leave","Leave with salary","Yes"},
                {"ThoughtWorks","training Leave","Leave without salary","Yes"}

        };
    }}






package GlobalInterfaceTest;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.GlobalInterface.ShiftConfigSteps;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 11/5/17.
 */
public class ShiftConfigTest extends DriverManager {

    ShiftConfigSteps shiftConfigSteps;


    public ShiftConfigTest() throws Exception
    {
        shiftConfigSteps =new ShiftConfigSteps();

    }


    @BeforeSuite
    public void initDriver() throws IOException {
        LoginPage page = new LoginSteps().Login(prop.getProperty("n"), prop.getProperty("p"));
        shiftConfigSteps.setGlobalInterface();
    }
    @Test(dataProvider = "SearchProvider")
    public void addshiftConfigurationTime(String  nam,String naml,String cli,String exe, String min) {

        shiftConfigSteps.setShiftConfiguration(nam, naml, cli, exe,  min);


        Assert.assertTrue(driver.getPageSource().contains("List Shift Configuration"));

        driver.findElement(By.xpath("//*[@id=\'search-container\']/a/i")).click();
    }






    @DataProvider(name = "SearchProvider")
    public static Object[][] getDataFromDataprovider() {
        return new Object[][]{
                {"10tSession","الجلسة الأولى","ThoughtWorks","10","06:00:00"},
                {"2ND  Session","الجلسة الأولى","ThoughtWorks","10","06:00:00"},

                {"3RD  Session","الجلسة الأولى","ThoughtWorks","10","06:00:00"},

                {"4TH  Session","الجلسة الأولى","ThoughtWorks","10","06:00:00"},

                {"5TH  Session","الجلسة الأولى","ThoughtWorks","10","06:00:00"},
                {"6TH  Session","الجلسة الأولى","ThoughtWorks","10","06:00:00"}


        };
    }}







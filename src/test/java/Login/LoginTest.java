package Login;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import steps.LoginSteps;
import util.DriverManager;

import java.io.IOException;



public class LoginTest extends DriverManager
{

    LoginSteps lSteps;

    int inputDataFlag = 0;
    // 0 -> Invalid i/p
    // 1 -> Valid inputs


    String[] successMsg;
    int successCounter = 0;

    String successText;

    public LoginTest() throws IOException {
        lSteps = new LoginSteps();
        successMsg = prop.getProperty("loginSuccess").split(",");
    }


    //    Invalid Credential as input
    @Test(dataProvider = "setData")
    public void initDriver(String name, String password) {
        //Arrange
        lSteps.Login(name, password);
        Assert.assertFalse(driver.getPageSource().contains("Hello Sony George"), "Valid credentials");
    }
    //   valid Credential as input
    @Test(priority = 1,dataProvider = "setData")
    public void init(String name,String password)
    {
        lSteps.Login(name,password);
        Assert.assertTrue(driver.getPageSource().contains(successMsg[successCounter]), "Valid credentials");


//logout
        if(driver.getPageSource().contains(successMsg[successCounter])){
            driver.findElement(By.xpath(" //*[@id=\"rightMenu\"]/li/a")).click();
            driver.findElement(By.linkText("Logout")).click();
            successCounter++;
        }

    }

    @DataProvider
    Object[][] setData() {

        Object[][] dataValues = getValues();

        return dataValues;
    }




    private String[][] getValues(){

        String[] namArray = new String[0];
        String[] pasArray = new String[0];

        if(inputDataFlag == 0){
            namArray = prop.getProperty("invalidnames").split(",");
            pasArray = prop.getProperty("invalidPwd").split(",");
        }else if(inputDataFlag == 1){
            namArray = prop.getProperty("validnames").split(",");
            pasArray = prop.getProperty("validPwd").split(",");
        }


        String nam, pas;

        int len = namArray.length;

        String[][] dataValues = new String[len][2];

        for (int i = 0; i < len; i++) {
            nam = namArray[i];
            pas = pasArray[i];

            dataValues[i][0] = nam;
            dataValues[i][1] = pas;

        }

        inputDataFlag++;

        return dataValues;

    }

    @AfterSuite
    public void close() {
        driver.close();
    }

}

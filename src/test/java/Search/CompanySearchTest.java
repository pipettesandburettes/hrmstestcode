package Search;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.LoginSteps;
import steps.Search.CompanySearchSteps;
import util.DriverManager;

import java.io.IOException;
import java.util.List;

/**
 * Created by naveen on 19/4/17.
 */
public class CompanySearchTest extends DriverManager {


    CompanySearchSteps comp;
    int allPageSearchCount = 0, totalSearchResultCount = 0;

    public CompanySearchTest() throws IOException {

        comp = new CompanySearchSteps();

    }

    @BeforeSuite
    public void initDRiver() throws IOException {
        LoginPage page = new LoginSteps().Login(prop.getProperty("n"), prop.getProperty("p"));
        comp.setTargetPage();
    }


    @Test(dataProvider = "SearchProvider")
    public void init(String nam, String add) {

        comp.searchAction(nam,add);
        checkSearch(nam,add, true);

        checkSearchesInAllPage(nam,add);

//        checkPagination();

    }


    private void checkSearch(String name,String addr, boolean isChecking) {
        //isChecking
        // true -> To check if the page contain only the search results
        // false -> To get the count of search results from whole data

        List<WebElement> rows = driver.findElements(By.className("li-row"));
        String searchValueName = "", searchValueAddress = "", nameValue, addrvalue;
        if(isChecking) {
            totalSearchResultCount = rows.size();
            System.out.println("Total Search Result: " + totalSearchResultCount);
        }
        int finalValue = (rows.size())+2;
        for (int pos = 2; pos < finalValue ; pos++) {
            if(addr.isEmpty()) {
                searchValueName = driver.findElement(By.xpath("//*[@id=\'list-container\']/ul/li[" + pos + "]/span[3]")).getText();
                nameValue = searchValueName.substring(0,name.length());
                if(isChecking) {
                    Assert.assertTrue(name.equalsIgnoreCase(nameValue));
                }else {
                    if(name.equalsIgnoreCase(nameValue))
                        allPageSearchCount++;
                }
            }else if(name.isEmpty() && !addr.isEmpty()) {
                searchValueAddress = driver.findElement(By.xpath("//*[@id=\'list-container\']/ul/li[" + pos + "]/span[4]")).getText();
                addrvalue = searchValueAddress.substring(0,addr.length());
                if(isChecking){
                    Assert.assertTrue(addr.equalsIgnoreCase(addrvalue));
                }else {
                    if(addr.equalsIgnoreCase(addrvalue))
                        allPageSearchCount++;
                }
            }else {
                searchValueName = driver.findElement(By.xpath("//*[@id=\'list-container\']/ul/li[" + pos + "]/span[3]")).getText();
                searchValueAddress = driver.findElement(By.xpath("//*[@id=\'list-container\']/ul/li[" + pos + "]/span[4]")).getText();
                nameValue = searchValueName.substring(0,name.length());
                addrvalue = searchValueAddress.substring(0,addr.length());
                if(isChecking) {
                    Assert.assertTrue((name.equalsIgnoreCase(nameValue) && addr.equalsIgnoreCase(addrvalue)));
                }else {
                    if(name.equalsIgnoreCase(nameValue) && addr.equalsIgnoreCase(addrvalue))
                        allPageSearchCount++;
                }

            }


        }

    }


    private void checkSearchesInAllPage(String searchName, String searchAddr) {

//         Clear search, check the search values with data with page nate

        String name="", addr="";
        comp.searchAction(name,addr);

//        checkSearch(searchName, searchAddr, false);

//        Page-nation
        String totalEntries = driver.findElement(By.xpath("//*[@id=\"list-container\"]/div/span")).getText();
        String[] splited = totalEntries.split("\\s+");

        String split_second=splited[5];
        float f=Float.parseFloat(split_second );
        float pagenum = f /15;
        int d = (int) Math.ceil(pagenum);

        for(int i=1;i<= d;i++) {
            driver.findElement(By.linkText(String.valueOf(i))).click();
            checkSearch(searchName, searchAddr, false);
        }

        System.out.println("Whole page count: "+allPageSearchCount);
        Assert.assertTrue(totalSearchResultCount == allPageSearchCount);

        allPageSearchCount = 0;

    }


    private void checkPagination(){
        String totalEntries = driver.findElement(By.xpath("//*[@id=\"list-container\"]/div/span")).getText();
        // print the total number of elements
//        System.out.println(totalEntries);


        String[] splited = totalEntries.split("\\s+");


        String split_second=splited[5];
//        System.out.println(split_second );
        float f=Float.parseFloat(split_second );
        float pagenum = f /15;
        int d = (int) Math.ceil(pagenum);


//        System.out.println(d);

        for(int i=2;i<= d;i++){
            driver.findElement(By.linkText(String.valueOf(i))).click();
            String url =driver.getCurrentUrl();
//            System.out.println(url);
            String[] paths = url.split("/");
            String indexString = paths[(paths.length)-1];
//            System.out.println(indexString);
            if(indexString.contains("?")) {
                String[] parameterArray = indexString.split("\\?");
                indexString = parameterArray[0];
            }
//            System.out.println(indexString);

            Assert.assertTrue(String.valueOf(i).equals(indexString));

        }
    }





    @DataProvider(name="SearchProvider")
    public  static Object[][]  getDataFromDataprovider(){
        return new Object[][] {
                { "a","i"},
                {"tele",""},
                {"t","f"}


        };
    }}



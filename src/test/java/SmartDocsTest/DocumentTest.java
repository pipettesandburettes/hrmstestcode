package SmartDocsTest;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.LoginSteps;
import steps.SmartDocs.DocumentSteps;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 5/4/17.
 */
public class DocumentTest extends DriverManager {

        DocumentSteps documentSteps;

    public DocumentTest() throws IOException {
        documentSteps =new DocumentSteps();

    }

    @BeforeSuite
    public void initDRiver() throws IOException {
        LoginPage page = new LoginSteps().Login(prop.getProperty("empuser"), prop.getProperty("emppas"));
        documentSteps.setSmartDoc();

    }
    @Test(dataProvider = "SearchProvider")
    public void setAddDocument(String nam,String fold,String cli, String sh,String lg,String shar,String per, String tg,String mt,String lk,String up) throws Exception {
        documentSteps.sDocumentNames(nam,fold, cli,sh,lg,shar, per, tg, mt, lk,up);



        Assert.assertTrue(driver.getPageSource().contains("List Documents"));

        driver.findElement(By.xpath("//*[@id=\'search-container\']/a/i")).click();
    }






    @DataProvider(name = "SearchProvider")
    public static Object[][] getDataFromDataprovider() {
        return new Object[][]{
                {"PAN CARD","/folder11","ThoughtWorks","testing","testing","Private","neetha123","test","test","test","/home/naveen/Videos/head-first-java-2nd-edition.pdf"},
                {"PASSPORT","/folder11","ThoughtWorks","testing","testing","Public read","naveen123","test","test","test","/home/naveen/Videos/head-first-java-2nd-edition.pdf"},
                {"AADHAR","/folder11","ThoughtWorks","testing","testing","Private","neetha123","test","test","test","/home/naveen/Videos/head-first-java-2nd-edition.pdf"},
                {"VOTERS ID","/folder11","ThoughtWorks","testing","testing","Private","cerin123","test","test","test","/home/naveen/Videos/head-first-java-2nd-edition.pdf"},

                {"DRIVING LICENSE","/folder11","ThoughtWorks","testing","testing","Private","naveen123","test","test","test","/home/naveen/Videos/head-first-java-2nd-edition.pdf"},
                {"VISA CARD","/folder11","ThoughtWorks","testing","testing","Public write","cerin123","test","test","test","/home/naveen/Videos/head-first-java-2nd-edition.pdf"}




        };
    }}
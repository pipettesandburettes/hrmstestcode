package SmartDocsTest;

import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.LoginSteps;
import steps.SmartDocs.FolderSteps;
import util.DriverManager;

import java.io.IOException;

/**
 * Created by naveen on 5/4/17.
 */
public class FolderTest extends DriverManager {

    FolderSteps folderSteps;

    public FolderTest() throws IOException {
        folderSteps=new FolderSteps();
    }

@BeforeSuite
    public void initDriver() throws IOException {
    LoginPage page = new LoginSteps().Login(prop.getProperty("n"), prop.getProperty("p"));
    folderSteps.setSmart();
}
@Test(dataProvider = "searchProvider")
    public void init(String nam,String parid,String cli,String sh,String per) throws InterruptedException {
    folderSteps.setAddFolderName(nam,parid,cli,sh,per);
    Assert.assertTrue(driver.getPageSource().contains("List Folders"));

}
@DataProvider(name ="searchProvider")
public static Object[][] getDataFromDataprovider() {
    return new Object[][]{
            {"folder123","/folder1","ThoughtWorks","Public write","naveen123"}



    };
}}







package Sorting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import pageobject.LoginPage;
import steps.LoginSteps;
import steps.Sorting.CompanySortSteps;
import util.DriverManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by naveen on 18/4/17.
 */
public class CompanySortTest extends DriverManager {

    CompanySortSteps comp;

    public CompanySortTest() throws IOException {
        comp = new CompanySortSteps();
    }

    @BeforeSuite
    public void initDRiver() throws IOException {
        LoginPage page = new LoginSteps().Login(prop.getProperty("n"), prop.getProperty("p"));
        comp.companySort();

    }

    @Test
    public void init() {
        comp.setCompanySort();
        List<WebElement> rows = driver.findElements(By.className("li-row"));
        int finalValue = (rows.size())+2;
        ArrayList<String> listValues = new ArrayList<>();
        boolean isSorted = true;
        for (int i = 2, j=(i-2); i < finalValue; i++) {
            String sortValueName = driver.findElement(By.xpath("//*[@id=\'list-container\']/ul/li["+i+"]/span[3]")).getText();
            listValues.add(sortValueName);

            if(j!=0){
                if(listValues.get(j-1).compareToIgnoreCase(listValues.get(j)) > 0){
                    isSorted = false;
                    break;
                }
            }

            j++;
        }

        Assert.assertTrue(isSorted);

    }
}



